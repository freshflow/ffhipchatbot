<?php

require dirname(__FILE__) . '/class.HipChatBot.php';
require dirname(__FILE__) . '/dibi-2.3.0/dibi/dibi.php';
require 'User.php';
require 'FreshFlowGateway.php';
//include dirname(__FILE__) . 'FreshFlowGateway.php';

// give it 1) your api token and 2) the room_id to monitor
$hcb = new HipChatBot('da9a4b78acf7fe234a3da7b3eff44a','1616293');

class Entry{
	
	public function checkType($val)
	{
		if (in_array('user', $val)) {
			$commands = "/commands/";
			$searchReg = "/(search|find) user (.*)/";
			$unblockBT_To_date_Reg = "/unblock user ([0-9]+|.*@.*\..*) to (.*)/"; 
			$unblockBTReg = "/unblock user ([0-9]+|.*@.*\..*)/";
			$str = implode(' ', $val);
			$out = "";			
			if(preg_match($searchReg, $str))
			{
				
				$user = new User();
				$out = $user->searchUser($val);
				return $out;
			}
			else if(preg_match($unblockBT_To_date_Reg,$str))
			{
				$options = array(
				    
				    'driver'   => 'mysql',
				    'host'     => 'vps46.nlooud.com',
				    'username' => 'gh42896800',
				    'password' => 'russokibo',
				    'database' => 'gh42896800db',
				    'charset'  => 'utf8',

				);
				$user = new User();
				$out = $user->unblockUserWithDate($val);
				return $out;
			}
			else if(preg_match($unblockBTReg, $str))
			{
				
				$options = array(
				    
				    'driver'   => 'mysql',
				    'host'     => 'vps46.nlooud.com',
				    'username' => 'gh42896800',
				    'password' => 'russokibo',
				    'database' => 'gh42896800db',
				    'charset'  => 'utf8',

				);
				$user = new User();
				$out = $user->unblockUser($val);
				return $out;
			}
			else
			{
				return "bad query syntax";
			}
		}

		else if(count($val) == 1 && $val[0] == "commands")
		{
			return 	"(find|search) user (Username|Uid|Partners_id|Name|Email)" . "<br />" .
					"unblock user (Email|uid)" . "<br />" .
					"unblock user (Email|uid) to dd-mm-yy"
			;
		}
		else if (in_array('event', $val)) {
  			return "search event";
		}

		else if (in_array('synchro', $val))
		{
			$u = 'silvie.klausova';
			$p = 'p12345o';
			$domain = 'https://mjoni.freshflow.cz';			

			$name = $val[2];
			$start = $val[3];
			/*if(isset($val[4]))
			{
				$end = $val[4];
			}
			else
			{
				$end = $start;
			}*/
			$end = $val[4];

			$data = array($name,$start,$end);

			$ffg = FreshFlowGateway::factory($u,$p,$domain);
			$res = $ffg->getDirectRequest('SynchroHelper','test',$data);
			//print_r($res);
			return $res;
		}

		else
			return "Error in query syntax";

		return "End";
	}
}


/* 
register some simple bots:  
 	1) call bot name  
 	2) bot display name  
 	3) function receiving args and returning what to send back to hipchat room 


*/

// simple bot that can return weather and emoticon info..
//  example typing ":bot weather" will return weather info from yahoo api 
$hcb->register_name_bot('bot1', 'Bot1', function($args) {

	$weather_url = 'http://query.yahooapis.com/v1/public/yql?format=json&q=select%20*%20from%20weather.forecast%20where%20location=94111';
	$emoticons = array('(hipchat)','(beer)','(coffee)','(poo)','(heart)',
					   '(itsatrap)','(lolwut)','(sadpanda)','(bumble)',
					   '(cornelius)','(thumbsup)','(thumbsdown)',
					   '(washington)','(lincoln)','(taft)','(jobs)',
					   '(fu)','(troll)','(megusta)','(okay)','(omg)',
					   '(foreveralone)','(sadtroll)','(yuno)','(cereal)',
					   '(awyeah)','(lol)','(facepalm)','(garret)',
					   '(chris)','(pete)','(huh)','(gtfo)');

	if ( isset($args[0]) && $args[0] == 'weather' ) {
		$response = json_decode(HipChatBot::_curl($weather_url),true);
		return $response['query']['results']['channel']['item']['description'];
	} elseif ( isset($args[0]) && $args[0] == 'emoticons' ) {
		$text = '';
		foreach ( array_chunk($emoticons,5) as $emoticon_chunk) {
			$text .= implode(' ',$emoticon_chunk);
			$text .= '<br />';
		}
		return $text;
	}
	return "invalid argument";
});

// very simple bot will return back the first argument or 'nothing given'
$hcb->register_name_bot('rms', 'RMS', function($args) {
	if ( isset($args[0]) && strlen($args[0]) ) {
		return 'you said "' . $args[0] . '"';
	}
	return 'nothing given';
});

// "see bot" looks for a certain string in a message and then does func if finds it...
$hcb->register_needle_bot('emacs', 'Jack', function($args) {
	return 'yes! emacs was mentioned';
});


$hcb->register_name_bot('rms', 'RMS', function($args) {
	if ( isset($args[0]) && strlen($args[0]) ) {
		return 'you said "' . $args[0] . '"';
	}
	return 'nothing given';
});

$hcb->register_name_bot('bot', 'BOT', function($args) {
	return Entry::checkType($args);
});


// start the bot up...
$hcb->run();
