<?php

//include dirname(__FILE__) . 'prado/framework/';

class FreshFlowGateway //extends TApplicationComponent
{
    const F_KEY = 'b02cd9a86e3ae84914c394b622d97058fefef3bd';
    const LOGIN_URL = '/index.php?json=login&plain=true&rid=token';
    const DIRECT_URL = '/index.php?json=direct';
    const FORM_URL = '/index.php?json=form';
    const VIEW_URL = '/index.php?json=view';
    const DOMAIN = 'https://devel.freshflow.dev';
    const LANG = 'en';

    private static $_instance=[];
    private $_token;
    private $_username;
    private $_password;
    private $_counter=0;
    private $_domain;
    private $_login;
    
    public  static function factory($u,$p,$domain=null)
    {
        $key = $u.'@'.$domain; 
        if(!isset(self::$_instance[$key]) || !self::$_instance[$key])
        {
            self::$_instance[$key] = new self($u, $p, $domain);            
        }
        return self::$_instance[$key];
    }
    
    private function __construct($u,$p, $domain=null) {
        $this->_username = $u;
        $this->_password = $p;
        $this->_domain = ($domain) ?: self::DOMAIN;
        $this->_login = $this->login();
        $this->_token = $this->_login->token;
    }

    public function getUserID() {
        return $this->_login->result->Uid;
    }

    public function getDomain() {
        return $this->_domain;
    }
    
    public function login()
    {
        $query = [ 'lang'=>self::LANG,'module'=>'login','username'=>$this->_username,'password'=>$this->_password];
        $res = json_decode($this->doRequest(self::LOGIN_URL,http_build_query($query)));
        //$this->_token = $res->token;
        return $res; //($res->success) ? $res->token : false;
    }
        
    public function getDirectRequest($action,$method,$data) {
        
        $json = json_encode([
            'method'=>$method,
            'action'=>$action,
            'data'=>$data,
            'tid'=>++$this->_counter,
            'type'=>'rpc'
            ]);         
        $res = json_decode($this->doRequest(self::DIRECT_URL, $json));
        return $res->result;
    }

    public function getViewRequest($view,$context,$data) {
        
        if (is_array($data)) {
            $data = (object) $data;
        }
        $data->view = $view;
        $data->context = $context;
        $json = http_build_query($data);
        return json_decode($this->doRequest(self::VIEW_URL, $json));
    }

    public function getFormRequest($view,$action,$data) {
        
        if (is_array($data)) {
            $data = (object) $data;
        }
        $data->view = $view;
        $data->action = $action;
        $json = http_build_query($data);
        $res = json_decode($this->doRequest(self::FORM_URL, $json));        
        return $res;
    }

    public function doHttpRequest($url,$json='',$method="POST") {
        $headers = [];
        if ($this->_token) {
            $headers[] = "X-FFTOKEN: {$this->_token}";
        }
        $headers[] = 'Content-Type: application/json';
        
        if ($method == 'POST') {
            $o = http_post_data($this->_domain.$url,$json,['headers'=>$headers]);
        }
        else {
            $o = http_post_get($this->_domain.$url,['headers'=>$headers]);
        }
        return $o;
        
    }


    public function doRequest($url,$json='',$method="POST") {

	   syslog(LOG_NOTICE,json_encode(['url'=>$this->_domain.$url,'request'=>$json]));

        $c = curl_init();
        
        //setup headers
        $headers = [];
        if ($this->_token) {
            $headers[] = "X-FFTOKEN: ".$this->_token;
        }
        //$headers[] = 'Content-Type: application/json';
                
	    curl_setopt($c,CURLOPT_URL,$this->_domain.$url) ;
        curl_setopt($c,CURLOPT_FOLLOWLOCATION, false) ;
        curl_setopt($c,CURLOPT_HTTPHEADER, $headers );
        curl_setopt($c,CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt($c,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_FAILONERROR, true);
        

        if ($method == 'POST') {
		curl_setopt($c,CURLOPT_POST, TRUE) ;
        curl_setopt($c,CURLOPT_POSTFIELDS, $json) ;
    	   } elseif ($method == 'PUT') {
		curl_setopt($c,CURLOPT_CUSTOMREQUEST, 'PUT') ;
		curl_setopt($c,CURLOPT_POSTFIELDS, $json) ;
	   } elseif ($method == 'DELETE') {
		curl_setopt($c,CURLOPT_CUSTOMREQUEST, 'DELETE') ;
	   }

//echo ">>> ".$json."\n";
        $o = curl_exec($c);
        $info = curl_getinfo($c);
	//   $code = curl_getinfo($c);

        if ($err = curl_errno($c)) {
		   $er = curl_error($c);
           syslog(LOG_ERR,json_encode(['error'=>$err,'request'=>$json,'headers'=>$headers]));
            throw new FJsonException($err.''.$er);
        }
	       $status = $info['http_code'];
           syslog(LOG_NOTICE,json_encode(['response'=>$o,'status'=>$status,'request'=>$json,'headers'=>$headers]));
        //   throw new FJsonException($url);
        curl_close($c);
//echo "<<< ".$o."\n";
        return $o;
    }
    
}


